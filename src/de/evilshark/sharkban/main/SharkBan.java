package de.evilshark.sharkban.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.evilshark.sharkban.commands.BanCommand;
import de.evilshark.sharkban.commands.CrashCommand;
import de.evilshark.sharkban.commands.IPCommand;
import de.evilshark.sharkban.commands.KickCommand;
import de.evilshark.sharkban.commands.LockDownCommand;
import de.evilshark.sharkban.commands.MuteCommand;
import de.evilshark.sharkban.commands.PlayerInfo;
import de.evilshark.sharkban.commands.TempBanCommand;
import de.evilshark.sharkban.commands.TempMuteCommand;
import de.evilshark.sharkban.commands.UnBanCommand;
import de.evilshark.sharkban.commands.UnMuteCommand;
import de.evilshark.sharkban.listener.BlockCommand;
import de.evilshark.sharkban.listener.ChatFilter;
import de.evilshark.sharkban.listener.LockDownListener;
import de.evilshark.sharkban.listener.LoginListener;
import de.evilshark.sharkban.listener.TestMute;

public class SharkBan extends JavaPlugin {
	
	public static SharkBan main;
	
	public static SharkBan getInstance() {
		return main;
	}
	
	public static String prefix = "�cSharkBans �8>> ";
	public static String argument = prefix+"�4FEHLER�8: �cBitte �berpr�fe die Argumente!";
	public static String console = prefix+"�4FEHLER�8: �cDieser Command kann nur von der Konsole benutzt werden!";
	public static String offline = prefix+"�4FEHLER�8: �cDieser Spieler wurde nicht gefunden!";
	public static String permission = prefix+"�4FEHLER�8: �cDu hast nicht gen�gend Rechte!";
	
	public static File file = new File("plugins/SharkBans", "bans.yml");
	public static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	public static File file2 = new File("plugins/SharkBans", "times.yml");
	public static FileConfiguration times = YamlConfiguration.loadConfiguration(file2);
	
	public static File file3 = new File("plugins/SharkBans", "mutes.yml");
	public static FileConfiguration mutes = YamlConfiguration.loadConfiguration(file3);
	
	public static File file4 = new File("plugins/SharkBans", "filter.yml");
	public static FileConfiguration filter = YamlConfiguration.loadConfiguration(file4);
	
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void onEnable() {
		
		main = this;
		loadConfig();
		
		if(!(file.exists())) {
			
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!(file2.exists())) {
			
			try {
				file2.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!(file3.exists())) {
			
			try {
				file3.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!(file4.exists())) {
			
			try {
				file4.createNewFile();
				List<String> FilterList = new ArrayList();
				FilterList.add("noob");
				filter.set(".Forbidden Phrases", FilterList);
				saveFilter();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Bukkit.getConsoleSender().sendMessage(prefix+"Das Plugin �cSharkBans �8von �6_EvilShark_ �8und �6Pikachu044 �8wurde �2gestartet�8!");
		
		for(Player online : Bukkit.getOnlinePlayers()) {
			
			if(online.hasPermission("sharkban.status")) {
				
				online.sendMessage(prefix+"Das Plugin �cSharkBans �8von �6_EvilShark_ �8und �6Pikachu044 �8wurde �2gestartet�8!");
			}
		}
		
		this.getCommand("tempban").setExecutor(new TempBanCommand());
		this.getCommand("ban").setExecutor(new BanCommand());
		this.getCommand("unban").setExecutor(new UnBanCommand());
		this.getCommand("mute").setExecutor(new MuteCommand());
		this.getCommand("tempmute").setExecutor(new TempMuteCommand());
		this.getCommand("unmute").setExecutor(new UnMuteCommand());
		this.getCommand("playerinfo").setExecutor(new PlayerInfo());
		this.getCommand("ip").setExecutor(new IPCommand());
		this.getCommand("kick").setExecutor(new KickCommand());
		this.getCommand("lockdown").setExecutor(new LockDownCommand());
		this.getCommand("chatfilter").setExecutor(new de.evilshark.sharkban.commands.ChatFilter());
		this.getCommand("crash").setExecutor(new CrashCommand());
		

		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new LoginListener(), this);
		pm.registerEvents(new LockDownListener(), this);
		pm.registerEvents(new TestMute(), this);
		pm.registerEvents(new ChatFilter(), this);
		pm.registerEvents(new BlockCommand(), this);
		
		
	}
	
	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	public static void saveBans() {
		try {
			cfg.save(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void loadSBans() {
		try {
			cfg.load(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void saveMutes() {
		try {
			mutes.save(file3);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void loadMutes() {
		try {
			mutes.load(file3);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void saveTimes() {
		try {
			times.save(file2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void loadTimes() {
		try {
			times.load(file2);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void saveFilter() {
		try {
			filter.save(file4);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void loadFilter() {
		try {
			filter.load(file4);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
