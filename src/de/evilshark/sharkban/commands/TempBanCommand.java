package de.evilshark.sharkban.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.BanManager;
import de.evilshark.sharkban.util.TempBanUnits;

public class TempBanCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkban.admin.tempban")) {
			
			if(args.length >= 4) {
				
				String playername = args[0];
				long value;
				
				try {
					value = Integer.valueOf(args[1]);
				} catch(NumberFormatException exe) {
					cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Bitte verwende eine Ganzzahl f�r die Zeitdauer!");
					return true;
				}
				
				if(value >= 500) {
					
					cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Die Zahl muss unter 500 liegen!");
					
					return true;
					
					
				}
				
				if(cs.getName().toLowerCase().equals("_evilshark_")) {
					
					String form = args[2];
					String reason = "";
					
					for(int i = 3; i < args.length; i++) {
						reason += args[i] + " ";
					}
					
					List<String> unitList = TempBanUnits.getUnitsAsString();
					
					if(unitList.contains(form.toLowerCase())) {
						
						TempBanUnits unit = TempBanUnits.getUnit(form);
						
						long seconds = value * unit.getToSecond();
						
						if(BanManager.isBanned(BanManager.getUUID(playername)) == false) {
							
							BanManager.tempBan(BanManager.getUUID(playername), playername, reason, cs.getName(), seconds);
							
							cs.sendMessage(SharkBan.prefix+"Du hast den Spieler �c"+playername+" �4TEMPOR�R �8gebannt!");
							cs.sendMessage(SharkBan.prefix+"UUID: �5"+BanManager.getUUID(playername));
							cs.sendMessage(SharkBan.prefix+"Dauer des Bans: �c "+value +" �5"+form+"�5s");
							
							return true;
						} else {
							cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Spieler ist bereits gebannt worden!");
							return true;
						}
						
					} else {
						cs.sendMessage(SharkBan.prefix+"�4cFEHLER: �8Diese Zeitform ist nicht bekannt!");
						
						return true;
						
					}
					
					
				} else {
					
					if(playername.toLowerCase().equals("_evilshark_") || playername.toLowerCase().equals("alphawolf2350") || playername.toLowerCase().equals("meisterchicken44")) {
						
						cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Es ist dir nicht erlaubt, diesen Spieler zu bannen!");
						
						return true;
						
					} else {
						
						String form = args[2];
						String reason = "";
						
						for(int i = 3; i < args.length; i++) {
							reason += args[i] + " ";
						}
						
						List<String> unitList = TempBanUnits.getUnitsAsString();
						
						if(unitList.contains(form.toLowerCase())) {
							
							TempBanUnits unit = TempBanUnits.getUnit(form);
							
							long seconds = value * unit.getToSecond();
							
							if(BanManager.isBanned(BanManager.getUUID(playername)) == false) {
								
								BanManager.tempBan(BanManager.getUUID(playername), playername, reason, cs.getName(), seconds);
								
								cs.sendMessage(SharkBan.prefix+"Du hast den Spieler �c"+playername+" �4TEMPOR�R �8gebannt!");
								cs.sendMessage(SharkBan.prefix+"UUID: �5"+BanManager.getUUID(playername));
								cs.sendMessage(SharkBan.prefix+"Dauer des Bans: �c "+value +" �5"+form+"�5s");
								
								return true;
							} else {
								cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Spieler ist bereits gebannt worden!");
								return true;
							}
							
						} else {
							cs.sendMessage(SharkBan.prefix+"�4cFEHLER: �8Diese Zeitform ist nicht bekannt!");
							
							return true;
							
						}
					
					
				}
			
					
				}
				
				
			} else {
				cs.sendMessage(SharkBan.argument);
			}
			
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		
		
		
		return true;
	}

}
