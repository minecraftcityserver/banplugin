package de.evilshark.sharkban.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.BanManager;
import de.evilshark.sharkban.util.MuteManager;

public class PlayerInfo implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkban.admin.playerinfo")) {
			
			
			if(args.length == 1) {
				
				String playername = args[0];
				String UUID = MuteManager.getUUID(playername);
				
				int MuteTimes = MuteManager.getTimes(UUID);
				int BanTimes = BanManager.times(UUID);
				
				if(BanManager.isBanned(UUID) == true) { // SPIELER IST GEBANNT
					
					if(MuteManager.isMuted(UUID)) { // SPIELER IST GEBANNT UND GEMUTET
						
						if(SharkBan.cfg.getString(UUID + ".type").equals("temp")) { // BAN IST TEMP
							
							if(SharkBan.mutes.getString(UUID + ".type").equals("temp")) {            // SPIELER IST GEBANNT UND GEMUTET, MUTE UND BANN TEMP
								
								cs.sendMessage("�2============= �4PLAYER INFO �2=============");
								cs.sendMessage("�6Gemutet: �2TRUE");
								cs.sendMessage("�6Gebannt: �2TRUE");
								cs.sendMessage("�6Name: �5"+playername);
								cs.sendMessage("�6UUID: �5"+UUID);
								cs.sendMessage("�2============= �4BAN INFO �2=============");
								cs.sendMessage("�6Typ: �4TEMPOR�R");
								cs.sendMessage("�6L�uft ab in: "+BanManager.getRemaning(UUID));
								cs.sendMessage("�6Banner: �5"+BanManager.getBanner(UUID));
								cs.sendMessage("�6Grund: �5"+BanManager.getReason(UUID));
								cs.sendMessage("�6Vorf�lle: �5"+BanTimes);
								cs.sendMessage("�2============= �4MUTE INFO �2=============");
								cs.sendMessage("�6Typ: �4TEMPOR�R");
								cs.sendMessage("�6L�uft ab in: "+MuteManager.getRemaning(UUID));
								cs.sendMessage("�6Banner: �5"+MuteManager.getMuter(UUID));
								cs.sendMessage("�6Grund: �5"+MuteManager.getReason(UUID));
								cs.sendMessage("�6Vorf�lle: �5"+MuteTimes);
								
								
								
							} else { // SPIELER IST GEBANNT UND GEMUTET,BANN TEMP
								
								cs.sendMessage("�2============= �4PLAYER INFO �2=============");
								cs.sendMessage("�6Gemutet: �2TRUE");
								cs.sendMessage("�6Gebannt: �2TRUE");
								cs.sendMessage("�6Name: �5"+playername);
								cs.sendMessage("�6UUID: �5"+UUID);
								cs.sendMessage("�2============= �4BAN INFO �2=============");
								cs.sendMessage("�6Typ: �4TEMPOR�R");
								cs.sendMessage("�6L�uft ab in: "+BanManager.getRemaning(UUID));
								cs.sendMessage("�6Banner: �5"+BanManager.getBanner(UUID));
								cs.sendMessage("�6Grund: �5"+BanManager.getReason(UUID));
								cs.sendMessage("�6Vorf�lle: �5"+BanTimes);
								cs.sendMessage("�2============= �4MUTE INFO �2=============");
								cs.sendMessage("�6Typ: �4PERMANENT");
								cs.sendMessage("�6Banner: �5"+MuteManager.getMuter(UUID));
								cs.sendMessage("�6Grund: �5"+MuteManager.getReason(UUID));
								cs.sendMessage("�6Vorf�lle: �5"+MuteTimes);
								
								
								
							}
						
							
							
							
							
						} else { //SPIELER IST GEBANNT UND GEMUTET
							
							if(SharkBan.mutes.getString(UUID + ".type").equals("temp")) { //SPIELER IST GEBANNT UND GEMUTET, MUTE TEMP
								
								cs.sendMessage("�2============= �4PLAYER INFO �2=============");
								cs.sendMessage("�6Gemutet: �2TRUE");
								cs.sendMessage("�6Gebannt: �2TRUE");
								cs.sendMessage("�6Name: �5"+playername);
								cs.sendMessage("�6UUID: �5"+UUID);
								cs.sendMessage("�2============= �4BAN INFO �2=============");
								cs.sendMessage("�6Typ: �4PERMANENT");
								cs.sendMessage("�6Banner: �5"+BanManager.getBanner(UUID));
								cs.sendMessage("�6Grund: �5"+BanManager.getReason(UUID));
								cs.sendMessage("�6Vorf�lle: �5"+BanTimes);
								cs.sendMessage("�2============= �4MUTE INFO �2=============");
								cs.sendMessage("�6Typ: �4TEMPOR�R");
								cs.sendMessage("�6L�uft ab in: "+MuteManager.getRemaning(UUID));
								cs.sendMessage("�6Banner: �5"+MuteManager.getMuter(UUID));
								cs.sendMessage("�6Grund: �5"+MuteManager.getReason(UUID));
								cs.sendMessage("�6Vorf�lle: �5"+MuteTimes);
								
								
							} else { //SPIELER IST GEBANNT UND GEMUTET
								 
								cs.sendMessage("�2============= �4PLAYER INFO �2=============");
								cs.sendMessage("�6Gemutet: �2TRUE");
								cs.sendMessage("�6Gebannt: �2TRUE");
								cs.sendMessage("�6Name: �5"+playername);
								cs.sendMessage("�6UUID: �5"+UUID);
								cs.sendMessage("�2============= �4BAN INFO �2=============");
								cs.sendMessage("�6Typ: �4PERMANENT");
								cs.sendMessage("�6Banner: �5"+BanManager.getBanner(UUID));
								cs.sendMessage("�6Grund: �5"+BanManager.getReason(UUID));
								cs.sendMessage("�6Vorf�lle: �5"+BanTimes);
								cs.sendMessage("�2============= �4MUTE INFO �2=============");
								cs.sendMessage("�6Typ: �4PERMANENT");
								cs.sendMessage("�6L�uft ab in: "+MuteManager.getRemaning(UUID));
								cs.sendMessage("�6Banner: �5"+MuteManager.getMuter(UUID));
								cs.sendMessage("�6Grund: �5"+MuteManager.getReason(UUID));
								cs.sendMessage("�6Vorf�lle: �5"+MuteTimes);
								
								
							}
							
							
							
							
						}
						
						
					} else { //SPIELER GEBANNT
						
						if(SharkBan.cfg.getString(UUID + ".type").equals("temp")) { // SPIELER GEBANNT, BANN TEMP
							
							cs.sendMessage("�2============= �4PLAYER INFO �2=============");
							cs.sendMessage("�6Gemutet: �4FALSE");
							cs.sendMessage("�6Gebannt: �2TRUE");
							cs.sendMessage("�6Name: �5"+playername);
							cs.sendMessage("�6UUID: �5"+UUID);
							cs.sendMessage("�2============= �4BAN INFO �2=============");
							cs.sendMessage("�6Typ: �4TEMPOR�R");
							cs.sendMessage("�6L�uft ab in: "+BanManager.getRemaning(UUID));
							cs.sendMessage("�6Banner: �5"+BanManager.getBanner(UUID));
							cs.sendMessage("�6Grund: �5"+BanManager.getReason(UUID));
							cs.sendMessage("�6Vorf�lle: �5"+BanTimes);
							
						} else { //SPIELER GEBANNT
							
							cs.sendMessage("�2============= �4PLAYER INFO �2=============");
							cs.sendMessage("�6Gemutet: �4FALSE");
							cs.sendMessage("�6Gebannt: �2TRUE");
							cs.sendMessage("�6Name: �5"+playername);
							cs.sendMessage("�6UUID: �5"+UUID);
							cs.sendMessage("�2============= �4BAN INFO �2=============");
							cs.sendMessage("�6Typ: �4PERMANENT");
							cs.sendMessage("�6Banner: �5"+BanManager.getBanner(UUID));
							cs.sendMessage("�6Grund: �5"+BanManager.getReason(UUID));
							cs.sendMessage("�6Vorf�lle: �5"+BanTimes);
							
							
						}
						
						
						
						
						
						
						
						
						
					}
					
				} else if(MuteManager.isMuted(UUID) == true) { // SPIELER GEMUTET
					
					if(SharkBan.mutes.getString(UUID + ".type").equals("temp")) { // SPIELER GEMUTET, temp
					
						cs.sendMessage("�2============= �4PLAYER INFO �2=============");
						cs.sendMessage("�6Gemutet: �2TRUE");
						cs.sendMessage("�6Gebannt: �4FALSE");
						cs.sendMessage("�6Name: �5"+playername);
						cs.sendMessage("�6UUID: �5"+UUID);
						cs.sendMessage("�2============= �4MUTE INFO �2=============");
						cs.sendMessage("�6Typ: �4TEMPOR�R");
						cs.sendMessage("�6L�uft ab in: "+MuteManager.getRemaning(UUID));
						cs.sendMessage("�6Banner: �5"+MuteManager.getMuter(UUID));
						cs.sendMessage("�6Grund: �5"+MuteManager.getReason(UUID));
						cs.sendMessage("�6Vorf�lle: �5"+MuteTimes);
						
					} else { // SPIELER GEMUTET
						
						cs.sendMessage("�2============= �4PLAYER INFO �2=============");
						cs.sendMessage("�6Gemutet: �2TRUE");
						cs.sendMessage("�6Gebannt: �4FALSE");
						cs.sendMessage("�6Name: �5"+playername);
						cs.sendMessage("�6UUID: �5"+UUID);
						cs.sendMessage("�2============= �4MUTE INFO �2=============");
						cs.sendMessage("�6Typ: �4PERMANENT");
						cs.sendMessage("�6Banner: �5"+MuteManager.getMuter(UUID));
						cs.sendMessage("�6Grund: �5"+MuteManager.getReason(UUID));
						cs.sendMessage("�6Vorf�lle: �5"+MuteTimes);
						
					}		
					
					
				} else {
					
					if(BanTimes == 0) {
						
						if(MuteTimes == 0) {
							
							cs.sendMessage(SharkBan.prefix+"�cF�r diesen Spieler gibt es keine Eintr�ge!");
							
						} else {
							
							cs.sendMessage("�2============= �4PLAYER INFO �2=============");
							cs.sendMessage("�6Gemutet: �4FALSE");
							cs.sendMessage("�6Gebannt: �4FALSE");
							cs.sendMessage("�6Name: �5"+playername);
							cs.sendMessage("�6UUID: �5"+UUID);
							cs.sendMessage("�2============= �4MUTE INFO �2=============");
							cs.sendMessage("�6Vorf�lle: �5"+MuteTimes);
							
						}
						
						
					} else {
						
						if(MuteTimes == 0) {
							
							cs.sendMessage("�2============= �4PLAYER INFO �2=============");
							cs.sendMessage("�6Gemutet: �4FALSE");
							cs.sendMessage("�6Gebannt: �4FALSE");
							cs.sendMessage("�6Name: �5"+playername);
							cs.sendMessage("�6UUID: �5"+UUID);
							cs.sendMessage("�2============= �4BANN INFO �2=============");
							cs.sendMessage("�6Vorf�lle: �5"+BanTimes);
							
						} else {
						
						cs.sendMessage("�2============= �4PLAYER INFO �2=============");
						cs.sendMessage("�6Gemutet: �4FALSE");
						cs.sendMessage("�6Gebannt: �4FALSE");
						cs.sendMessage("�6Name: �5"+playername);
						cs.sendMessage("�6UUID: �5"+UUID);
						cs.sendMessage("�2============= �4BANN INFO �2=============");
						cs.sendMessage("�6Vorf�lle: �5"+BanTimes);
						cs.sendMessage("�2============= �4MUTE INFO �2=============");
						cs.sendMessage("�6Vorf�lle: �5"+MuteTimes);
						}
						
					}
					
					
				}
				
				
			} else {
				cs.sendMessage(SharkBan.argument);
				cs.sendMessage(SharkBan.prefix+"�6/playerinfo [Player]");
			}
			
			
			
			
			
		} else {
			
			cs.sendMessage("sharkban.admin.playerinfo");
			
		}
		
		
		
		return true;
	}

}
