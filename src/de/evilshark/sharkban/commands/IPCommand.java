package de.evilshark.sharkban.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;

public class IPCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkban.admin.ip")) {
			
			if(args.length == 1) {
				
				String playername = args[0];
				
				if(Bukkit.getPlayer(playername) != null) {
					
					cs.sendMessage(SharkBan.prefix+"IP von �5"+playername+": �5"+Bukkit.getPlayer(playername).getPlayer().getAddress().toString().substring(1));
					
				} else {
					cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Spieler ist nicht online!");
				}
				
				
			} else {
				cs.sendMessage(SharkBan.argument);
				cs.sendMessage(SharkBan.prefix+"�6/ip [Player]");
			}
			
			
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}

		
		return true;
	}
	
	

}
