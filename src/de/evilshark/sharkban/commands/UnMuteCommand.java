package de.evilshark.sharkban.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.MuteManager;

public class UnMuteCommand implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkban.admin.unmute")) {
			
			if(args.length == 1) {
				
				String playername = args[0];
				
				if(MuteManager.isMuted(MuteManager.getUUID(playername)) == true) {
					
					MuteManager.unMute(MuteManager.getUUID(playername));
					cs.sendMessage(SharkBan.prefix+"Du hast den Spieler "+playername+" �2entmutet�8!");
					cs.sendMessage(SharkBan.prefix+"UUID: "+MuteManager.getUUID(playername));
					
				} else {
					cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Dieser Spieler ist nicht gemutet!");
					return true;
				}
				
				
			} else {
				cs.sendMessage(SharkBan.argument);
				cs.sendMessage(SharkBan.prefix+"�6/unmute [Player]");
			}
			
			
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		
		
		
		
		return true;
		
	}

}
