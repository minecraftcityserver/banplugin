package de.evilshark.sharkban.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkban.main.SharkBan;

public class LockDownCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

		if(cs.hasPermission("sharkban.command.lockdown")) {
			
			if(args.length >= 0) {
				
				if(args.length == 0) {
					
					boolean isEnabled = SharkBan.main.getConfig().getBoolean("Config.lockdown");
					
					if(isEnabled == true) {
						
						SharkBan.main.getConfig().set("Config.lockdown", false);
						SharkBan.main.getConfig().set("Config.lockdownreason", "Wartungsarbeiten");
						SharkBan.main.saveConfig();
						cs.sendMessage(SharkBan.prefix+"Der Wartungsmodus wurde �4deaktiviert�8!");
						
					} else {
						
						SharkBan.main.getConfig().set("Config.lockdown", true);
						SharkBan.main.getConfig().set("Config.lockdownreason", "Wartungsarbeiten");
						SharkBan.main.saveConfig();
						
						cs.sendMessage(SharkBan.prefix+"Der Wartungsmodus wurde �2aktiviert�8!");
						
						for(Player online : Bukkit.getOnlinePlayers()) {
							
							if(!(online.hasPermission("sharkban.admin.lockdown"))) {
								
								online.kickPlayer("�cDu wurdest vom Server gekickt!\n" 
								+ "\n" 
								+ "�2Grund: �cWartungsarbeiten");
								
							}
							
						}
						
						
					}
					
					
				} else {
					
					String reason = "";
					
					for(int i = 0; i < args.length; i++) {
						reason += args[i] + " ";
					}
					
					boolean isEnabled = SharkBan.main.getConfig().getBoolean("Config.lockdown");
					
					if(isEnabled == true) {
						
						SharkBan.main.getConfig().set("Config.lockdown", false);
						SharkBan.main.getConfig().set("Config.lockdownreason", "Wartungsarbeiten");
						SharkBan.main.saveConfig();
						cs.sendMessage(SharkBan.prefix+"Der Wartungsmodus wurde �4deaktiviert�8!");
						
					} else {
						
						SharkBan.main.getConfig().set("Config.lockdown", true);
						SharkBan.main.getConfig().set("Config.lockdownreason", reason);
						SharkBan.main.saveConfig();
						
						cs.sendMessage(SharkBan.prefix+"Der Wartungsmodus wurde �2aktiviert�8!");
						
						for(Player online : Bukkit.getOnlinePlayers()) {
							
							if(!(online.hasPermission("sharkban.admin.lockdown"))) {
								
								online.kickPlayer("�cDu wurdest vom Server gekickt!\n" 
								+ "\n" 
								+ "�2Grund: �c"+reason);
								
							}
							
						}
						
						
					}
					
					
					
					
				}
				
				
				
				
			} else {
				
				cs.sendMessage(SharkBan.argument);
				cs.sendMessage(SharkBan.prefix+"�6/lockdown (Reason)");
				
			}
			
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		

		return true;
	}
	
	

}
