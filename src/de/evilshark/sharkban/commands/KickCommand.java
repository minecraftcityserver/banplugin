package de.evilshark.sharkban.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.BanManager;

public class KickCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkban.admin.kick")) {
			
			if(args.length >= 2) {
				
				String playername = args[0];
				String reason = "";
				
				if(Bukkit.getPlayer(playername) != null) {
					
					for(int i = 1; i < args.length; i++) {
						reason += args[i] + " ";
					}
					
					if(cs.getName().toLowerCase().equals("_evilshark_")) {
						
						cs.sendMessage(SharkBan.prefix+"Du hast den Spieler �4"+playername+" �8vom Server �cgekickt!");
						cs.sendMessage(SharkBan.prefix+"�6Grund: �c"+reason);
						cs.sendMessage(SharkBan.prefix+"�1UUID: �c"+BanManager.getUUID(playername));
						
						Bukkit.getPlayer(playername).kickPlayer("�cDu wurdest vom Server gekickt\n" + 
						
						"\n" +
								
						"�2Grund: �c"+reason + "\n" +
						"\n" +
						"");	
						
					} else {
						
						if(playername.toLowerCase().equals("_evilshark_") || playername.toLowerCase().equals("alphawolf2350") || playername.toLowerCase().equals("meisterchicken44")) {
							
							cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Du darfst diesen Spieler nicht kicken!");
							
						} else {
							
							cs.sendMessage(SharkBan.prefix+"Du hast den Spieler �4"+playername+" �8vom Server �cgekickt!");
							cs.sendMessage(SharkBan.prefix+"�6Grund: �c"+reason);
							cs.sendMessage(SharkBan.prefix+"�1UUID: �c"+BanManager.getUUID(playername));
							
							Bukkit.getPlayer(playername).kickPlayer("�cDu wurdest vom Server gekickt\n" + 
							
							"\n" +
									
							"�2Grund: �c"+reason + "\n" +
							"\n" +
							"");	
						}
						
					}
					

					
				} else {
					
					cs.sendMessage(SharkBan.offline);
					
					return true;
					
				}
				
				
				
				
			} else {
				
				cs.sendMessage(SharkBan.argument);
				cs.sendMessage(SharkBan.prefix+"�6/kick [Player] [Reason]");
				
				return true;
				
			}
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		
		
		
		return true;
	}
	
	

}
