package de.evilshark.sharkban.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.BanManager;

public class UnBanCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkban.admin.unban")) {
			
			if(args.length == 1) {
				
				String playername = args[0];
				
				if(BanManager.isBanned(BanManager.getUUID(playername)) == true) {
					
					BanManager.unBan(BanManager.getUUID(playername));
					cs.sendMessage(SharkBan.prefix+"Der Spieler wurde �2entbannt!");
					cs.sendMessage(SharkBan.prefix+"UUID: "+BanManager.getUUID(playername));
					
				} else {
					
					cs.sendMessage(SharkBan.prefix+"�4FEHLER �8: Der Spieler ist nicht gebannt!");
					return true;
				}
				
				
			} else {
				cs.sendMessage(SharkBan.argument);
			}
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		
		
		
		
		return true;
	}
	
	

}
