package de.evilshark.sharkban.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.MuteManager;
import de.evilshark.sharkban.util.TempBanUnits;

public class TempMuteCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkban.admin.tempmute")) {
			
			if(args.length >= 4) {
				
				String playername = args[0];
				long value;
				
				try {
					value = Integer.valueOf(args[1]);
				} catch(NumberFormatException exe) {
					cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Bitte verwende eine Ganzzahl f�r die Zeitdauer!");
					return true;
				}
				
				if(value >= 500) {
					
					cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Die Zahl muss unter 500 liegen!");
					
					return true;
					
					
				}
				
				String form = args[2];
				String reason = "";
				
				for(int i = 3; i < args.length; i++) {
					reason += args[i] + " ";
				}
				
				List<String> unitList = TempBanUnits.getUnitsAsString();
				
				if(unitList.contains(form.toLowerCase())) {
					
					TempBanUnits unit = TempBanUnits.getUnit(form);
					
					long seconds = value * unit.getToSecond();
					
					if(MuteManager.isMuted(MuteManager.getUUID(playername)) == false) {
						
						MuteManager.tempMute(MuteManager.getUUID(playername), playername, seconds, reason, cs.getName());
						
						cs.sendMessage(SharkBan.prefix+"Du hast den Spieler �c"+playername+" �4TEMPOR�R �8gemutet!!");
						cs.sendMessage(SharkBan.prefix+"UUID: �5"+MuteManager.getUUID(playername));
						cs.sendMessage(SharkBan.prefix+"Dauer des Chatbanns: "+MuteManager.getRemaning(MuteManager.getUUID(playername)));
						
						return true;
					} else {
						cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Spieler ist bereits gemutet worden!");
						return true;
					}
					
				} else {
					cs.sendMessage(SharkBan.prefix+"�4cFEHLER: �8Diese Zeitform ist nicht bekannt!");
				}
				
				
			} else {
				
				cs.sendMessage(SharkBan.argument);
				cs.sendMessage(SharkBan.prefix+"�6/tempmute [Player] [Time] [Timeform] [Reason]");
				
			}
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		 
		
		
		
		return true;
	}
	
	

}
