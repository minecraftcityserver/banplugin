package de.evilshark.sharkban.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.evilshark.sharkban.main.SharkBan;

public class ChatFilter implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
	
		if(cs.hasPermission("sharkban.admin.chatfilter")) {
			
			if(args.length == 1) {
				
				if(args[0].equalsIgnoreCase("enable")) {
					
					boolean status = SharkBan.main.getConfig().getBoolean("Config.enableFilter");
					
					if(status == false) {
						 
						SharkBan.main.getConfig().set("Config.enableFilter", true);
						SharkBan.main.saveConfig();
						
						cs.sendMessage(SharkBan.prefix+"Du hast den Chatfilter �2aktiviert!");
						
					} else {
						
						cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Chatfilter ist bereits aktiv!");
						
					}
					
				} else if(args[0].equalsIgnoreCase("disable")) {
					
					boolean status = SharkBan.main.getConfig().getBoolean("Config.enableFilter");
					
					if(status == true) {
						
						SharkBan.main.getConfig().set("Config.enableFilter", false);
						SharkBan.main.saveConfig();
						
						cs.sendMessage(SharkBan.prefix+"Du hast den Chatfilter �4deaktiviert!");
						
					} else {
						
						cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Chatfilter nicht aktiv!");
						
					}
					
				} else if(args[0].equalsIgnoreCase("list")) {
					
					SharkBan.loadFilter();
					
					cs.sendMessage(SharkBan.prefix+"Momentan sind folgende Phrasen �cblockiert:");
					cs.sendMessage(SharkBan.filter.getStringList(".Forbidden Phrases").toString());
										
					
				} else {
					
					cs.sendMessage(SharkBan.argument);
					cs.sendMessage(SharkBan.prefix+"�6/chatfilter [list/enable/disable/add/remove/setmaxwarns/info]");
					
				}
				
			} else if(args.length == 2) {
				
				if(args[0].equalsIgnoreCase("add")) {
					
					String phrase = args[1].toLowerCase();
					
					de.evilshark.sharkban.util.ChatFilter.addFilter(phrase, cs);
					
				} else if(args[0].equalsIgnoreCase("remove")) {
					
					String phrase = args[1].toLowerCase();
					
					de.evilshark.sharkban.util.ChatFilter.removeFilter(phrase, cs);
					
				} else if(args[0].equalsIgnoreCase("info")) {
					
					Player p = Bukkit.getPlayer(args[1]);
					
					if(SharkBan.times.contains(p.getUniqueId().toString())) {
						
						int warns = SharkBan.times.getInt(p.getUniqueId().toString() + ".chatFilter");
						
						cs.sendMessage(SharkBan.prefix+"Der Spieler �5"+args[1]+" �8hat �6"+warns+" �8Verwarnungen!");
						
					} else {
						
						cs.sendMessage(SharkBan.prefix+"�cF�r diesen Spieler gibt es keine Eintr�ge!");
						
					}
					
				} else if(args[0].equalsIgnoreCase("setmaxwarns")) {
					
					try {
						
						int max = Integer.parseInt(args[1]);
						
						SharkBan.main.getConfig().set("Config.maxWarnings", max);
						SharkBan.main.saveConfig();
						
						cs.sendMessage(SharkBan.prefix+"Du hast die maximalen Verwarnungen auf �5"+ max + " �8gesetzt!");
										
					} catch(Exception exe) {
						
						cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Es wurde eine ung�ltige Zahl eingegeben!");
						
					}
					
					
					
				} else {
					
					cs.sendMessage(SharkBan.argument);
					cs.sendMessage(SharkBan.prefix+"�6/chatfilter [list/enable/disable/add/remove/setmaxwarns/info]");
					
				}
				
				
				
			} else {
				
				cs.sendMessage(SharkBan.argument);
				cs.sendMessage("�6/chatfilter [list/enable/disable/add/remove/setmaxwarns/info]");
				
			}
			
			
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		
		
		
		
		return true;
	}
	
	

}
