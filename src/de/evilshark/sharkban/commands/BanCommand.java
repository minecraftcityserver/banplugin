package de.evilshark.sharkban.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.BanManager;

public class BanCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
	
		
		if(cs.hasPermission("sharkban.admin.ban")) {
			
			if(args.length >= 2) {
				
				String playername = args[0];
				String reason = "";
				
				if(cs.getName().toLowerCase().equalsIgnoreCase("_evilshark_")) {
					
					for(int i = 1; i < args.length; i++) {
						reason += args[i] + " ";
					}
					
					if(BanManager.isBanned(BanManager.getUUID(playername)) == false) {
						
						BanManager.ban(BanManager.getUUID(playername), playername, reason, cs.getName());
						
						cs.sendMessage(SharkBan.prefix+"Du hast den Spieler �c"+playername+" �4PERMANTENT �8gebannt!");
						cs.sendMessage(SharkBan.prefix+"UUID: �5"+BanManager.getUUID(playername));
					} else {
						cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Spieler wurde bereits gebannt!");
						return true;
					}
					
				} else {
					
					if(playername.toLowerCase().equals("_evilshark_") || playername.toLowerCase().equals("alphawolf2350") || playername.toLowerCase().equals("meisterchicken44")) {
						
						cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Es ist dir nicht erlaubt, diesen Spieler zu bannen!");
						
						return true;
						
					} else {
						
						for(int i = 1; i < args.length; i++) {
							reason += args[i] + " ";
						}
						
						if(BanManager.isBanned(BanManager.getUUID(playername)) == false) {
							
							BanManager.ban(BanManager.getUUID(playername), playername, reason, cs.getName());
							
							cs.sendMessage(SharkBan.prefix+"Du hast den Spieler �c"+playername+" �4PERMANTENT �8gebannt!");
							cs.sendMessage(SharkBan.prefix+"UUID: �5"+BanManager.getUUID(playername));
						} else {
							cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Spieler wurde bereits gebannt!");
							return true;
						}
					
				}
					
				}
				

				
			} else {
				cs.sendMessage(SharkBan.argument);
			}
			
			
			
			
			
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		
		return true;
		
		

	}
	
	

}
