package de.evilshark.sharkban.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.MuteManager;

public class MuteCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(cs.hasPermission("sharkban.admin.mute")) {
			
			if(args.length >= 2) {
				
				String playername = args[0];
				String reason = "";
				
				for(int i = 1; i < args.length; i++) {
					reason += args[i] + " ";
				}
				
				if(MuteManager.isMuted(MuteManager.getUUID(playername)) == false) {
					
					MuteManager.mute(MuteManager.getUUID(playername), playername, reason, cs.getName());
					
					cs.sendMessage(SharkBan.prefix+"Du hast den Spieler "+playername+" �4PERMANENT �cgemutet!");
					cs.sendMessage(SharkBan.prefix+"UUID: "+MuteManager.getUUID(playername));
					
				} else {
					cs.sendMessage(SharkBan.prefix+"�4FEHLER: �8Der Spieler wurde bereits gemutet!");
					return true;
				}
				
				
			} else {
				cs.sendMessage(SharkBan.argument);
				cs.sendMessage(SharkBan.prefix+"�6/mute [Player] [Reason]");
			}
			
			
			
			
		} else {
			
			cs.sendMessage(SharkBan.permission);
			
		}
		
		return true;
	}
	
	

}
