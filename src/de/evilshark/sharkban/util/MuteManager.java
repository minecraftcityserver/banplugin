package de.evilshark.sharkban.util;

import org.bukkit.Bukkit;

import de.evilshark.sharkban.main.SharkBan;

public class MuteManager {
	
	public static void tempMute(String uuid, String playername, long time, String reason, String muter) {
		
		long current = System.currentTimeMillis();
		long millis = time * 1000;
		long end = current + millis;
		
		SharkBan.mutes.set(uuid + ".name", playername);
		SharkBan.mutes.set(uuid + ".reason", reason);
		SharkBan.mutes.set(uuid + ".banner", muter);
		SharkBan.mutes.set(uuid + ".time", end);
		SharkBan.mutes.set(uuid + ".type", "temp");
		SharkBan.times.set(uuid + ".MuteTimes", getTimes(uuid) + 1);
		
		SharkBan.saveTimes();
		SharkBan.saveMutes();
		
		if(Bukkit.getPlayer(playername) != null) {
		
			Bukkit.getPlayer(playername).sendMessage(SharkBan.prefix+"�cDu wurdest �4TEMPOR�R �caus dem Chat gebannt!");
			Bukkit.getPlayer(playername).sendMessage(SharkBan.prefix+"�2Verbleibene Zeit: "+getRemaning(uuid));
			Bukkit.getPlayer(playername).sendMessage(SharkBan.prefix+"�5Grund: �c"+getReason(uuid));
			
		}
		
	}
	
	public static void mute(String uuid, String playername, String reason, String muter) {
		
		SharkBan.mutes.set(uuid + ".name", playername);
		SharkBan.mutes.set(uuid + ".reason", reason);
		SharkBan.mutes.set(uuid + ".banner", muter);
		SharkBan.mutes.set(uuid + ".type", "perm");
		SharkBan.times.set(uuid + ".MuteTimes", getTimes(uuid) + 1);
		
		SharkBan.saveTimes();
		SharkBan.saveMutes();
		
		if(Bukkit.getPlayer(playername) != null) {
			
			Bukkit.getPlayer(playername).sendMessage(SharkBan.prefix+"�cDu wurdest �4PERMANENT �caus dem Chat gebannt!");
			Bukkit.getPlayer(playername).sendMessage(SharkBan.prefix+"�5Grund: �c"+getReason(uuid));			
		}
	}
	
	public static void unMute(String uuid) {
		
		SharkBan.mutes.set(uuid, null);
		SharkBan.saveMutes();
	}
	
	public static boolean isMuted(String uuid) {
		
		SharkBan.loadMutes();
		
		if(SharkBan.mutes.contains(uuid)) {
			
			return true;
			
		} else {
			return false;
		}
		
		
	}
	
	public static String getReason(String uuid) {
		
		SharkBan.loadMutes();
		
		return SharkBan.mutes.getString(uuid + ".reason");
	}
	
	public static long getEnd(String uuid) {
		
		SharkBan.loadMutes();
		
		return SharkBan.mutes.getLong(uuid + ".time");
		
	}
	
	public static String getRemaning(String uuid) {
		
		long current = System.currentTimeMillis();
		long end = getEnd(uuid);
		long millis = end - current;
		
		long seconds = 0;
		long minutes = 0;
		long hours = 0;
		long days = 0;
		long weeks = 0;
		long months = 0;
		
		while(millis > 1000) {
			millis -= 1000;
			seconds++;
		}
		
		while(seconds > 60) {
			seconds -= 60;
			minutes++;
		}
		
		while(minutes > 60) {
			minutes -= 60;
			hours++;
		}
		
		while(hours > 24) {
			hours -= 24;
			days++;
		}
		
		while(days > 7) {
			days -= 7;
			weeks++;
		}
		
		while(weeks > 4) {
			weeks -= 4;
			months++;
		}
		
		return  "�e "+ months+ "Monat(e) " + weeks + " Woche(n) " + days + " Tag(e) " + hours + " Stunde(n) " + minutes + " Minute(n) " + seconds + " Sekunde(n)";
		
		
	}
	
	public static String getMuter(String uuid) {
		
		SharkBan.loadMutes();
		
		return SharkBan.mutes.getString(uuid + ".banner");
		
	}
	
	public static int getTimes(String uuid) {
		
		SharkBan.loadTimes();
		
		return SharkBan.times.getInt(uuid + ".MuteTimes");
		
	}
	
	@SuppressWarnings("deprecation")
	public static String getUUID(String playername) {
		
		return Bukkit.getOfflinePlayer(playername).getUniqueId().toString();
		
	}
	

}
