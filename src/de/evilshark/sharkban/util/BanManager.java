package de.evilshark.sharkban.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.evilshark.sharkban.main.SharkBan;

public class BanManager {
	
public static void tempBan(String uuid, String name, String reason, String banner, long lenght) {
		
		long current = System.currentTimeMillis();
		long millis = lenght * 1000;
		long end = current + millis;
			
			SharkBan.cfg.set(uuid + ".name", name);
			SharkBan.cfg.set(uuid + ".reason", reason);
			SharkBan.cfg.set(uuid + ".banner", banner);
			SharkBan.cfg.set(uuid + ".time", end);
			SharkBan.cfg.set(uuid + ".type", "temp");
			SharkBan.times.set(uuid + ".BanTimes", times(uuid) + 1);
			
			SharkBan.saveTimes();
			SharkBan.saveBans();
			
			if(Bukkit.getPlayer(name) != null) {
				
				Bukkit.getPlayer(name).kickPlayer("�cDu wurdest �4�lTEMPOR�R �cvom Server gebannt!\n" + 
				"\n" +
				"�2Entbannung in: �e" + getRemaning(uuid) + "\n" +
				"\n" +
				"�bGrund: �c"+ getReason(uuid) + "\n" +
				"\n" +
				"");	
			}
		
	}
	
	public static void ban(String uuid, String name, String reason, String banner) {
		
			
			SharkBan.cfg.set(uuid + ".name", name);
			SharkBan.cfg.set(uuid + ".reason", reason);
			SharkBan.cfg.set(uuid + ".banner", banner);
			SharkBan.cfg.set(uuid + ".type", "perm");
			SharkBan.times.set(uuid + ".BanTimes", times(uuid) + 1);
			
			SharkBan.saveTimes();
			SharkBan.saveBans();
			
			if(Bukkit.getPlayer(name) != null) {
				
				Bukkit.getPlayer(name).kickPlayer("�cDu wurdest �l�4PERMANENT �cvom Server gebannt!\n" + 
				"\n" +
				"�2Entbannung: �4�lNIE" + "\n" +
				"\n" +
				"�bGrund: �c"+ getReason(uuid) + "\n" +
				"\n" +
				"");	
			}
			
		
		
	}
	
	public static boolean isBanned(String uuid) {
		
		SharkBan.loadSBans();
		
		if(SharkBan.cfg.contains(uuid)) {
			
			return true;
			
	} else {
		return false;
	}
}
	
	public static void unBan(String uuid) {
		
			SharkBan.cfg.set(uuid , null);
			
			SharkBan.saveBans();

	}
	
	public static String getReason(String uuid) {
		
		if(isBanned(uuid) == true) {
			
			SharkBan.loadSBans();
			
			String reason = SharkBan.cfg.getString(uuid + ".reason");
			return reason;
			
		} else {
			return null;
		}
		
	}
	
	public static void banInfo(Player p, String uuid) {
		
	}
	
	public static long getEnd(String uuid) {
		
		SharkBan.loadSBans();
		
		return SharkBan.cfg.getLong(uuid + ".time");
		
	}
	
	
	public static String getRemaning(String uuid) {
		
		long current = System.currentTimeMillis();
		long end = getEnd(uuid);
		long millis = end - current;
		
		long seconds = 0;
		long minutes = 0;
		long hours = 0;
		long days = 0;
		long weeks = 0;
		long months = 0;
		
		while(millis > 1000) {
			millis -= 1000;
			seconds++;
		}
		
		while(seconds > 60) {
			seconds -= 60;
			minutes++;
		}
		
		while(minutes > 60) {
			minutes -= 60;
			hours++;
		}
		
		while(hours > 24) {
			hours -= 24;
			days++;
		}
		
		while(days > 7) {
			days -= 7;
			weeks++;
		}
		
		while(weeks > 4) {
			weeks -= 4;
			months++;
		}
		
		return  "�e " +months + " Monat(e)"+ weeks + " Woche(n) " + days + " Tag(e) " + hours + " Stunde(n) " + minutes + " Minute(n) " + seconds + " Sekunde(n)";
		
		
	}
	
	public static String getBanner(String uuid) {
		
		if(isBanned(uuid) == true) {
			
			SharkBan.loadSBans();
			
			String banner = SharkBan.cfg.getString(uuid + ".banner");
			
			return banner;
			
			
		} else {
			return null;
		}
		
		
		
	}
	
	public static int times(String uuid) {
		
		SharkBan.loadTimes();
		
		return SharkBan.times.getInt(uuid + ".BanTimes");
		
	}
	
	@SuppressWarnings("deprecation")
	public static String getUUID(String playername) {
		
		return Bukkit.getOfflinePlayer(playername).getUniqueId().toString();
		
	}

}
