package de.evilshark.sharkban.util;

import java.util.ArrayList;
import java.util.List;

public enum TempBanUnits {
	
	SECOND("Sekunde(n)", 1, "second"),
	MINUTE("Minute(n)", 60, "minute"),
	HOUR("Stunde(n)", 60*60, "hour"),
	DAY("Tag(e)", 24*60*60, "day"),
	WEEK("Woche(n)", 7*24*60*60, "week"),
	MONTH("Monat(e)", 4*7*24*60*60, "month");
	
	private String name;
	private int toSecond;
	private String shortcut;
	
	private TempBanUnits(String name, int toSecond, String shortcut) {
		
		this.name = name;
		this.toSecond = toSecond;
		this.shortcut = shortcut;
		
	}
	
	public int getToSecond() {
		return toSecond;
	}
	
	public String getName() {
		return name;
	}
	
	public String getShortCut() {
		return shortcut;
	}
	
	
	public static List<String> getUnitsAsString() {
		
		List<String> units = new  ArrayList<String>();
		for(TempBanUnits unit : TempBanUnits.values()) {
			units.add(unit.getShortCut().toLowerCase());
		}
		
		return units;
		
	}
	
	public static TempBanUnits getUnit(String unit) {
		for(TempBanUnits units : TempBanUnits.values()) {
			if(units.getShortCut().toLowerCase().equals(unit.toLowerCase())) {
				return units;
			}
		}
		
		return null;
		
	}

}
