package de.evilshark.sharkban.util;

import java.util.List;

import org.bukkit.command.CommandSender;

import de.evilshark.sharkban.main.SharkBan;

public class ChatFilter {
	  
	  public static void addFilter(String forbidden, CommandSender p) {
		  
		  String phrase = forbidden.toLowerCase();
		  
		  List<String> FilterList = SharkBan.filter.getStringList("Forbidden Phrases");
		  
		  SharkBan.loadFilter();
		  
		  if(FilterList.contains(phrase)) {
			  
			  p.sendMessage(SharkBan.prefix+"§4FEHLER: §8Diese Phrase wurde bereits hinzugefügt!");
			  
		  } else {
			  
			  FilterList.add(phrase);
			  SharkBan.filter.set(".Forbidden Phrases", FilterList);
			  SharkBan.saveFilter();
			  
			  p.sendMessage(SharkBan.prefix+"Du hast die Phrase §5"+forbidden+" §8erfolgreich hinzugefügt!");
			  
		  }
		  
	  }
	  
	  public static void removeFilter(String forbidden, CommandSender p) {
		  
		 String phrase = forbidden.toLowerCase();
		 
		 List<String> FilterList = SharkBan.filter.getStringList("Forbidden Phrases");
		 
		 SharkBan.loadFilter();
		 
		 if(FilterList.contains(phrase)) {
			 
			 FilterList.remove(phrase);
			 SharkBan.filter.set(".Forbidden Phrases", FilterList);
			 SharkBan.saveFilter();
			 
			 p.sendMessage(SharkBan.prefix+"Du hast die Phrase §5"+forbidden+" §8erfolgreich entfernt!");
			 
		 } else {
			 
			 p.sendMessage(SharkBan.prefix+"§4FEHLER: §8Diese Phrase wurde noch nicht hinzugefügt!");
			 
		 }
		  
		  
	  }
	  
	  public static List<String> getList() {
		  
		  SharkBan.loadFilter();
		  
		  return SharkBan.filter.getStringList("Forbidden Phrases");
		  
	  }
	  
	  public static boolean isFiltered(String msg) {
		  
		  SharkBan.loadFilter();
		  
		  for(String words : SharkBan.filter.getStringList("Forbidden Phrases")) {
			  
			  //return msg.contains(words) ? true : false;
			  
			  if(msg.contains(words)) {
				  
				  return true;
				  
			  }
			  
			  
		  }
		return true;
				  
		  
	  }
	  
	  
	  

}
