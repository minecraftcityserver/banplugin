package de.evilshark.sharkban.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.MuteManager;

public class ChatFilter implements Listener {
	
	@EventHandler
	public void chatFilter(AsyncPlayerChatEvent e) {
		
		String msg = e.getMessage().toLowerCase();
		Player p = e.getPlayer();
		
		if(MuteManager.isMuted(p.getUniqueId().toString()) == false) {
			
			if(!(p.hasPermission("sharkban.chatfilter.ignore"))) {
				
				for(String words : SharkBan.filter.getStringList("Forbidden Phrases")) {
					
					if(msg.contains(words)) {
						
						int maxWarnings = SharkBan.main.getConfig().getInt("Config.maxWarnings");
						int playerwarnings = SharkBan.times.getInt(p.getUniqueId().toString() + ".chatFilter");
						
						SharkBan.times.set(p.getUniqueId().toString() + ".chatFilter", playerwarnings + 1);
						SharkBan.saveTimes();
						playerwarnings++;
						
						if(playerwarnings < maxWarnings) {
							
							p.sendMessage(SharkBan.prefix+"�cBitte achte auf deine Schreibweise!");
							p.sendMessage(SharkBan.prefix+"�cVerwarnung: �6"+ playerwarnings +" �cvon �6"+maxWarnings);
							
							e.setCancelled(true);
							
						} else {
							
							SharkBan.loadMutes();
							
							
							String banner = "SYSTEM";
							String reason = "Nicht erlaubte Schreibweise";
							String UUID = MuteManager.getUUID(p.getName());
							int muteTimes = SharkBan.times.getInt(UUID + ".MuteTimes");

							
							p.sendMessage(SharkBan.prefix+"�cDu hast die maximale Anzahl an Verwarnungen erhalten!");
							
							if(muteTimes == 0) {
								
								MuteManager.tempMute(UUID, p.getName(), 10800, reason, banner);
								SharkBan.times.set(UUID + ".MuteTimes", muteTimes + 1);
								SharkBan.times.set(UUID + ".chatFilter", 0);
								SharkBan.saveTimes();
								
							} else if(muteTimes == 1) {
								
								MuteManager.tempMute(UUID, p.getName(), 86400, reason, banner);
								SharkBan.times.set(UUID + ".MuteTimes", muteTimes + 1);
								SharkBan.times.set(UUID + ".chatFilter", 0);
								SharkBan.saveTimes();
								
							} else if(muteTimes == 2) {
								
								MuteManager.tempMute(UUID, p.getName(), 604800, reason, banner);
								SharkBan.times.set(UUID + ".MuteTimes", muteTimes + 1);
								SharkBan.times.set(UUID + ".chatFilter", 0);
								SharkBan.saveTimes();
								
							} else if(muteTimes == 3) {
								
								MuteManager.tempMute(UUID, p.getName(), 2419200, reason, banner);
								SharkBan.times.set(UUID + ".MuteTimes", muteTimes + 1);
								SharkBan.times.set(UUID + ".chatFilter", 0);
								SharkBan.saveTimes();
								
							} else {
								
								MuteManager.mute(UUID, p.getName(), reason, banner);
								SharkBan.times.set(UUID + ".MuteTimes", muteTimes + 1);
								SharkBan.times.set(UUID + ".chatFilter", 0);
								SharkBan.saveTimes();
								
							}
							
							
						}
						
						
						
						
						
						
						e.setCancelled(true);
						
					}
					
				}
			
		}
			
	} else {
		
		e.setCancelled(true);
	}
		
		
		
	}

}
