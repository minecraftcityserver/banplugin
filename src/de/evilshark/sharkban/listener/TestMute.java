package de.evilshark.sharkban.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.evilshark.sharkban.main.SharkBan;
import de.evilshark.sharkban.util.MuteManager;

public class TestMute implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent e) {
		
		SharkBan.loadMutes();
		Player p = e.getPlayer();
		String UUID = MuteManager.getUUID(p.getName());
		String type = SharkBan.mutes.getString(UUID + ".type");
		
		if(MuteManager.isMuted(UUID) == true) {
			
			if(type.equals("temp")) {
				
				long current = System.currentTimeMillis();
				long end = MuteManager.getEnd(UUID);

				
				if(current < end) {
					
					e.setCancelled(true);
					p.sendMessage(SharkBan.prefix+"�cDu wurdest �4TEMPOR�R �caus dem Chat gebannt!");
					p.sendMessage(SharkBan.prefix+"�2Verbleibene Zeit: "+MuteManager.getRemaning(UUID));
					p.sendMessage(SharkBan.prefix+"�5Grund: �c"+MuteManager.getReason(UUID));
					
				} else {
					
					SharkBan.mutes.set(UUID, null);
					SharkBan.saveMutes();
					p.sendMessage(SharkBan.prefix+"Dein Chatbann wurde aufgehoben!");
					
				}
				
			} else {
				
				e.setCancelled(true);
				p.sendMessage(SharkBan.prefix+"�cDu wurdest �4PERMANENT �caus dem Chat gebannt!");
				p.sendMessage(SharkBan.prefix+"�5Grund: �c"+MuteManager.getReason(UUID));
				
			}
			
			
		}
		
		
	}

}
