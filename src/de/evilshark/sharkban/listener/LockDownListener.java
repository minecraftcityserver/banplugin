package de.evilshark.sharkban.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import de.evilshark.sharkban.main.SharkBan;

public class LockDownListener implements Listener {

	@EventHandler
	public void onJoin(PlayerLoginEvent e) {
		
		Player p = e.getPlayer();
		
	
		boolean isEnabled = SharkBan.main.getConfig().getBoolean("Config.lockdown");
		String reason = SharkBan.main.getConfig().getString("Config.lockdownreason");
		
		if(isEnabled == true) {
			
			if(!(p.hasPermission("sharkban.admin.lockdown"))) {
				
				p.kickPlayer("�cDu kannst den Server momentan nicht betreten!\n" 
				+ "\n" 
				+ "�2Grund: �c"+reason);
				
				
			}
			
			
		}
		
		
	}
	
	
	
}
